.PHONY: all, render_html, render_pdf, ci.render_html, ci.render_pdf, help
.DEFAULT_GOAL := help

ifneq (,$(wildcard ./.env))
    include .env
    export
endif

IN ?= index.md
OUT ?= $(basename $(IN))

# Command to run decktape
PANDOC_CMD = pandoc
DECKTAPE_CMD = decktape


help:
	@grep -E '^[a-zA-Z._-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

all: render_html

# # OR to update many presentations at once:
# all:
# 	@$(MAKE) render_html IN=presentation1.md
# 	@$(MAKE) render_html IN=presentation2.md

render_html: ## Render presentation to revealjs slideshow with pandoc
	@$(PANDOC_CMD) \
		-t revealjs \
		-o $(OUT).html \
		-V colorlinks=true \
		-V theme=white \
		-V slideNumber=true \
		-V progress=true \
		--css static/style.css \
		--standalone \
		--slide-level=2 \
		--include-in-header=static/header_include.html \
		$(IN)

$(OUT).html: render_html

render_pdf: $(OUT).html ## Render presentation to pdf with decktape
	@$(DECKTAPE_CMD) $(OUT).html $(OUT).pdf

########################################################
################# CI Specific recipes ##################
########################################################

ci.render_html: public/$(OUT).html ## [CI] Renders html in public repository for gitlab pages.

ci.render_pdf: public/$(OUT).pdf ## [CI] Renders pdf in public repository for gitlab pages.

public/$(OUT).html: public/_redirects public/static public/images
	@ $(MAKE) --no-print-directory render_html OUT=public/$(OUT)


public/$(OUT).pdf: | public/$(OUT).html public/_redirects public/static public/images
# In CI, we want to avoid recomputing public/$(OUT).html (pandoc not
# available at this stage), that's why we put prerequisites as "order-only"
# and we don't invoke "render_pdf" as above.
	@$(DECKTAPE_CMD) public/$(OUT).html public/$(OUT).pdf

public/_redirects:
	@mkdir -p public
	@echo '/$(CI_PROJECT_NAME)  /$(CI_PROJECT_NAME)/$(OUT).html' > public/_redirects
	@echo '/*/$(CI_PROJECT_NAME)/  /:splat/$(CI_PROJECT_NAME)/$(OUT).html' >> public/_redirects

public/static:
	@mkdir -p public
	@cp -r static public/static

public/images:
	@mkdir -p public
	@cp -r images public/images

