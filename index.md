---
title: Une coopérative du numérique d'intérêt général
subtitle: Ça fait quoi, ça sert à quoi, et à qui ?
author: Johan Richer
date: <a href="https://alposs.fr/" target="_blank">AlpOSS</a>, Échirolles, le 21 mars 2024
title-slide-attributes:
    data-background-image: "./images/multi-logo.svg"
    data-background-size: "auto 10%, auto 12%"
    data-background-position: "95% 95%, 70% 97%"
---

# Fondation

:::::::::::::: {.columns}
::: {.column width="50%"}
<img src="./images/jailbreak-logo.png">
::: 

::: {.column width="50%"}
* Création en 2017 par des anciens d'[Etalab](https://etalab.gouv.fr/) et d'[Easter-eggs](https://www.easter-eggs.com/)
* _« Des **logiciels libres** pour des **données libres** »_
:::
::::::::::::::

# Seconde fondation 

:::::::::::::: {.columns}
::: {.column width="50%"}
<img src="./images/multi-logo.svg">
::: 

::: {.column width="50%"}
* Transformation en coopérative en 2022
* _« **Coopérative** du **numérique d'intérêt général** »_
:::
::::::::::::::

# Aujourd'hui

10 personnes dont **4 isérois‧e‧s** (bientôt 5 !) ⛰️

:::::::::::::: {.columns}

::: {.column width="20%"}
<img src="./images/thomas.png"><br><img src="./images/thibaut.png">
:::

::: {.column width="20%"}
<img src="./images/quentin.png"><br><img src="./images/erica.jpg">
:::

::: {.column width="20%"}
<img src="./images/sarra.jpg"><br><img src="./images/pierre.jpg">
:::

::: {.column width="20%"}
<img src="./images/julien.png"><br><img src="./images/johan.jpg">
:::

::: {.column width="20%"}
<img src="./images/amelie.jpg"><br><img src="./images/tarik.png">
:::

::::::::::::::

# Une coopérative du numérique d'intérêt général

* Un **véhicule** : la coopérative
* Une **destination** : le numérique d'intérêt général

# On fait quoi ?

## L'expertise multi

* Des **services** et **produits** numériques
* Spécialisation sur les données et les logiciels libres
* Développement web, data science, data engineering
* Produit
* Coaching stratégique

## Pour quoi, pour qui ?

**Services publics** numériques

---

![](./images/cassos.png)


_Les infiltrés_ - Matthieu Aron & Caroline Michel-Aguirre (2023)

---

:::::::::::::: {.columns}

::: {.column width="33%"}
<img src="./images/datagouv.png">
::: 

::: {.column width="30%"}
<img src="./images/schemadatagouv.png">
:::

::: {.column width="33%"}
<img src="./images/validata.png">
::: 

::::::::::::::

:::::::::::::: {.columns}

::: {.column width="33%"}
<img src="./images/betagouv.png">
::: 

::: {.column width="33%"}
<img src="./images/meteodatagouv.png">
::: 

::: {.column width="30%"}
<img src="./images/ecologiedatagouv.png">
:::

::::::::::::::

---

![](./images/datami.png)

## « Numérique d'intérêt général » ?

* Une raison d'être ?
* Tentative de définition

# On fait comment ?

## Au fait, c'est quoi une coopérative ?

* Alignement avec nos valeurs
* Avantages pour les travailleur‧se‧s du numérique
* Inter-coopération

## « Une méthode multi » ?

Problème : comment vendre et appliquer _vraiment_ des pratiques agiles dans les contraintes de l'administration ?

# Une coopérative du numérique d'intérêt général

* Un **véhicule** : la coopérative
* Une **destination** : le numérique d'intérêt général

_« La route est longue, mais la voie est libre... »_

# Avec vous ?

# Merci !

**Johan Richer**  
Président  
[twitter.com/JohanRicher](https://twitter.com/JohanRicher)  
[johan.richer@multi.coop](mailto:johan.richer@multi.coop)
